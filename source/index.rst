.. Vim documentation master file, created by
   sphinx-quickstart on Sat Sep 30 11:05:47 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vim Guide
=========

.. toctree::
   :maxdepth: 3
   :numbered:
   :includehidden:
   :caption: Contents:

   vim


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

